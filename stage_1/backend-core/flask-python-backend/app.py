#!/usr/bin/env python
from distutils.log import debug
#from xml.dom.xmlbuilder import _DOMBuilderErrorHandlerType
from flask import Flask, render_template, request, jsonify
from flask_cors import CORS
from bson.objectid import ObjectId
from datetime import datetime
import pika
import uuid
import json
import yaml
import logging


######################################################## RPC-CLIENT MONGO GET SECTION
class MongoRpcClient(object):

    def __init__(self):

        credentials = pika.PlainCredentials('root', 'password')
        parameters = pika.ConnectionParameters('rabbitmq', 5672, '/', credentials)

        self.connection = pika.BlockingConnection(parameters)


        self.channel = self.connection.channel()

        result = self.channel.queue_declare(queue='', exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self, rpc_req):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key=rpc_req,
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
            ),
            body=rpc_req)
        while self.response is None:
            self.connection.process_data_events()
        return self.response

########################################################



app = Flask(__name__)

CORS(app)


@app.route('/distr/')
def index():
    return render_template('home.html', title='STOP', content='Hello there! What are you doing here?')


@app.route('/distr/users', methods=['POST', 'GET'])
def dataUser():
    print("POST || GET")
    if request.method == 'GET':
        mongo_rpc = MongoRpcClient()
        rpc_req = 'rpc_queue_mongo_get'

        print(" [x] Sending request %r" % rpc_req)
        logging.info(" [x] Sending request %r" % rpc_req)
        dataJson = []
        response = mongo_rpc.call(rpc_req)
        print(" [x] Got response from GET-service.")
        print(response)
        for document in json.loads(response):
            print(" [.] Got %r" % document)
        allData = json.loads(response)
        for data in allData:
            id = data['_id']
            firstName = data['firstName']
            lastName = data['lastName']
            emailId = data['emailId']
            age = data['age']
            dataDict = {
                'id': str(id),
                'firstName': firstName,
                'lastName': lastName,
                'emailId': emailId,
                'age': age
            }
            dataJson.append(dataDict)
        return jsonify(dataJson)    
    
    

@app.route('/distr/users/<string:id>', methods=['GET', 'DELETE', 'PUT'])
def oneDataUser(id):
    print("GET || DELETE || PUT")

####################################################################### POSTS SECTION
@app.route('/distr/posts', methods=['POST', 'GET'])
def dataPost():
    print("POST || GET")

@app.route('/distr/posts/<string:id>', methods=['GET', 'DELETE', 'PUT'])
def oneDataPost(id):
    print("GET || DELETE || PUT")


####################################################################### COMMENTS SECTION
@app.route('/distr/comments', methods=['POST', 'GET'])
def dataComment():
    print("POST || GET")

@app.route('/distr/comments/<string:id>', methods=['GET', 'DELETE', 'PUT'])
def oneDataComment(id):
    print("GET || DELETE || PUT")

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000, debug=True)