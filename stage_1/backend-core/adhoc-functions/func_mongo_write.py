#!/usr/bin/env python

# Copyright (c) 2017 ObjectLabs Corporation
# Distributed under the MIT license - http://opensource.org/licenses/MIT

# Written with pymongo-3.4
# Documentation: http://docs.mongodb.org/ecosystem/drivers/python/
# A python script connecting to a MongoDB given a MongoDB Connection URI.

import sys
from gridfs import Database
import pymongo

### Create seed data

SEED_DATA = [
    {
        'decade': '1970s',
        'artist': 'Debby Boone',
        'song': 'You Light Up My Life',
        'weeksAtOne': 10
    },
    {
        'decade': '1980s',
        'artist': 'Olivia Newton-John',
        'song': 'Physical',
        'weeksAtOne': 10
    },
    {
        'decade': '1990s',
        'artist': 'Mariah Carey',
        'song': 'One Sweet Day',
        'weeksAtOne': 16
    },
    {
        'wrong_key': 'efwef',
        'wrong_key2': 'efewf',
        'lol': 'kek'
    }
]

### Standard URI format: mongodb://[dbuser:dbpassword@]host:port/dbname

uri = 'mongodb://root:password@mongo:27017/test?authSource=admin&authMechanism=SCRAM-SHA-256' 

###############################################################################
# main
###############################################################################

def main(args):

    client = pymongo.MongoClient(uri)
    # client = pymongo.MongoClient('mongo',
    #             username='root',
    #             password='example',
    #             authSource='admin',
    #             authMechanism='SCRAM-SHA-256')
    db = client.get_default_database()
    
    # First we'll add a few songs. Nothing is required to create the songs 
    # collection; it is created automatically when we insert.

    songs = db['songs']

    # Note that the insert method can take either an array or a single dict.

    songs.insert_many(SEED_DATA)

    # Then we need to give Boyz II Men credit for their contribution to
    # the hit "One Sweet Day".
    client.close()


if __name__ == '__main__':
    main(sys.argv[1:])