#!/usr/bin/env python
#!/usr/bin/env python
import pika, sys, os, bsonjs
from pymongo import MongoClient
from bson.raw_bson import RawBSONDocument
from bson import json_util
from gridfs import Database
import json
import logging
import yaml

credentials = pika.PlainCredentials('root', 'password')
parameters = pika.ConnectionParameters('rabbitmq', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='rpc_queue_mongo_get')
config = yaml.safe_load(open('database.yaml'))
client = MongoClient(config['uri'])


def mongo_get(data):
    print('[x] mongo_get: working on...')
    config = yaml.safe_load(open('database.yaml'))
    client = MongoClient(config['uri'])
    db = client[config['database']]
    users = db['users']
    cursor = users.find({})
    array = []
    for document in cursor:
        array.append(document)
        print(document)
    client.close()
    return json.loads(json_util.dumps(array))

def on_request(ch, method, props, body):

    print(" [.] mongo will (%s)" % body)
    logging.info('Got request from core.')
    response = mongo_get(body)
    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = \
                                                         props.correlation_id),
                     body=json.dumps(response))
    ch.basic_ack(delivery_tag=method.delivery_tag)
    print('Request is valid.')
    logging.info('Request is valid.') 

channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='rpc_queue_mongo_get', on_message_callback=on_request)

print(" [x] Awaiting RPC requests")
logging.info('Awaiting RPC requests...')
channel.start_consuming()






