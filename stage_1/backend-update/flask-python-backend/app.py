#!/usr/bin/env python
from distutils.log import debug
#from xml.dom.xmlbuilder import _DOMBuilderErrorHandlerType
from flask import Flask, render_template, request, jsonify
from flask_graphql import GraphQLView
from flask_cors import CORS
from pymongo import MongoClient
from bson.objectid import ObjectId
from graphql_modules.schema import schema
from mongoengine import connect
from datetime import datetime

import yaml

app = Flask(__name__)
config = yaml.safe_load(open('database.yaml'))
client = MongoClient(config['uri'])
# client = MongoClient('mongo',
#                      username='root',
#                      password='ComingTrue1312!',
#                      authSource='admin',
#                      authMechanism='SCRAM-SHA-256')

connect(config['database'], host='mongodb://config:ComingTrue1312!@mongo:27017', alias='default')

db = client[config['database']] # DATABASE TO CONNECT
CORS(app)

###################################
###################################

default_query = '''
{
  allUsers {
    edges {
      node {
        id,
        firstName,
        }
    }
  }
}'''.strip()

app.add_url_rule(
    '/graphql',
    view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=True)
)

###################################



@app.route('/')
def index():
    return render_template('home.html', title='STOP', content='Hello there! What are you doing here?')


@app.route('/users', methods=['POST', 'GET'])
def dataUser():
    
    # POST a data to database
    if request.method == 'POST':
        payload = request.json
        firstName = payload['firstName']
        lastName = payload['lastName']
        emailId = payload['emailId'] 
        age = payload['age']
        db['users'].insert_one({
            "firstName": firstName,
            "lastName": lastName,
            "emailId": emailId,
            "age": age
        })
        return jsonify({
            'status': 'Data is posted to MongoDB!',
            'firstName': firstName,
            'lastName': lastName,
            'emailId': emailId,
            'age': age
        })
    
    # GET all data from database
    if request.method == 'GET':
        allData = db['users'].find()
        dataJson = []
        for data in allData:
            id = data['_id']
            firstName = data['firstName']
            lastName = data['lastName']
            emailId = data['emailId']
            age = data['age']
            dataDict = {
                'id': str(id),
                'firstName': firstName,
                'lastName': lastName,
                'emailId': emailId,
                'age': age
            }
            dataJson.append(dataDict)
        print(dataJson)
        return jsonify(dataJson)

@app.route('/users/<string:id>', methods=['GET', 'DELETE', 'PUT'])
def oneDataUser(id):

    # GET a specific data by id
    if request.method == 'GET':
        data = db['users'].find_one({'_id': ObjectId(id)})
        id = data['_id']
        firstName = data['firstName']
        lastName = data['lastName']
        emailId = data['emailId']
        age = data['age']
        dataDict = {
            'id': str(id),
            'firstName': firstName,
            'lastName': lastName,
            'emailId':emailId,
            'age': age
        }
        print(dataDict)
        return jsonify(dataDict)
        
    # DELETE a data
    if request.method == 'DELETE':
        db['users'].delete_many({'_id': ObjectId(id)})
        print('\n # Deletion successful # \n')
        return jsonify({'status': 'Data id: ' + id + ' is deleted!'})

    # UPDATE a data by id
    if request.method == 'PUT':
        payload = request.json
        firstName = payload['firstName']
        lastName = payload['lastName']
        emailId = payload['emailId']
        age = payload['age']

        db['users'].update_one(
            {'_id': ObjectId(id)},
            {
                "$set": {
                    "firstName":firstName,
                    "lastName":lastName,
                    "emailId": emailId,
                    "age": age
                }
            }
        )

        print('\n # Update successful # \n')
        return jsonify({'status': 'Data id: ' + id + ' is updated!'})

####################################################################### POSTS SECTION
@app.route('/posts', methods=['POST', 'GET'])
def dataPost():
    
    # POST a data to database
    if request.method == 'POST':
        payload = request.json
        title = payload['title']
        body = payload['body']
        author = payload['author']
        date = datetime.utcnow()
        db['posts'].insert_one({
            "title": title,
            "body": body,
            "author": author,
            "date": date
        })
        return jsonify({
            'status': 'Data is posted to MongoDB!',
            'title': title,
            'body': body,
            'author': author,
            'date': date
        })
    
    # GET all data from database
    if request.method == 'GET':
        allData = db['posts'].find()
        dataJson = []
        for data in allData:
            id = data['_id']
            title = data['title']
            body = data['body']
            author = data['author']
            date = data['date']
            dataDict = {
                'id': str(id),
                'title': title,
                'body': body,
                'author': str(author),
                'date': date
            }
            dataJson.append(dataDict)
        print(dataJson)
        return jsonify(dataJson)

@app.route('/posts/<string:id>', methods=['GET', 'DELETE', 'PUT'])
def oneDataPost(id):

    # GET a specific data by id
    if request.method == 'GET':
        data = db['posts'].find_one({'_id': ObjectId(id)})
        id = data['_id']
        title = data['title']
        body = data['body']
        author = data['author']
        date = data['date']
        dataDict = {
            'id': str(id),
            'title': title,
            'body': body,
            'author': str(author),
            'date': date
        }
        print(dataDict)
        return jsonify(dataDict)
        
    # DELETE a data
    if request.method == 'DELETE':
        db['posts'].delete_many({'_id': ObjectId(id)})
        print('\n # Deletion successful # \n')
        return jsonify({'status': 'Data id: ' + id + ' is deleted!'})

    # UPDATE a data by id
    if request.method == 'PUT':
        payload = request.json
        title = payload['title']
        body = payload['body']

        db['posts'].update_one(
            {'_id': ObjectId(id)},
            {
                "$set": {
                    "title": title,
                    "body":body
                }
            }
        )

        print('\n # Update successful # \n')
        return jsonify({'status': 'Data id: ' + id + ' is updated!'})


####################################################################### COMMENTS SECTION
@app.route('/comments', methods=['POST', 'GET'])
def dataComment():
    
    # POST a data to database
    if request.method == 'POST':
        payload = request.json
        text = payload['text']
        author = payload['author']
        post = payload['post']
        date = datetime.utcnow()
        db['comments'].insert_one({
            "text": text,
            "author": author,
            "post": post,
            "date": date
        })
        return jsonify({
            'status': 'Data is posted to MongoDB!',
            'text': text,
            'author': author,
            'post': post,
            'date': date
        })
    
    # GET all data from database
    if request.method == 'GET':
        allData = db['comments'].find()
        dataJson = []
        for data in allData:
            id = data['_id']
            text = data['text']
            author = data['author']
            post = data['post']
            date = data['date']
            dataDict = {
                'id': str(id),
                'text': text,
                'author': author,
                'post': str(post),
                'date': date
            }
            dataJson.append(dataDict)
        print(dataJson)
        return jsonify(dataJson)

@app.route('/comments/<string:id>', methods=['GET', 'DELETE', 'PUT'])
def oneDataComment(id):

    # GET a specific data by id
    if request.method == 'GET':
        data = db['comments'].find_one({'_id': ObjectId(id)})
        id = data['_id']
        text = data['text']
        author = data['author']
        post = data['post']
        date = data['date']
        dataDict = {
            'id': str(id),
            'text': text,
            'author': author,
            'post': str(post),
            'date': date
        }
        print(dataDict)
        return jsonify(dataDict)
        
    # DELETE a data
    if request.method == 'DELETE':
        db['comments'].delete_many({'_id': ObjectId(id)})
        print('\n # Deletion successful # \n')
        return jsonify({'status': 'Data id: ' + id + ' is deleted!'})

    # UPDATE a data by id
    if request.method == 'PUT':
        payload = request.json
        text = payload['text']

        db['comments'].update_one(
            {'_id': ObjectId(id)},
            {
                "$set": {
                    "text": text,
                }
            }
        )

        print('\n # Update successful # \n')
        return jsonify({'status': 'Data id: ' + id + ' is updated!'})

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000, debug=True)