#!/usr/bin/env python
import pika, sys, os, pymongo
from gridfs import Database
import json

credentials = pika.PlainCredentials('root', 'password')
parameters = pika.ConnectionParameters('rabbitmq', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='rpc_queue_mongo_set')
uri = 'mongodb://root:password@mongo:27017/test?authSource=admin&authMechanism=SCRAM-SHA-256' 

def mongo_write(data):
    client = pymongo.MongoClient(uri)
    db = client.get_default_database()
    songs = db['songs']
    songs.insert_many(json.loads(data))
    client.close()
    return "Please check!"

def on_request(ch, method, props, body):

    print(" [.] mongo will write(%s)" % body)
    response = mongo_write(body)
    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = \
                                                         props.correlation_id),
                     body=str(response))
    ch.basic_ack(delivery_tag=method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='rpc_queue_mongo_set', on_message_callback=on_request)

print(" [x] Awaiting RPC requests")
channel.start_consuming()