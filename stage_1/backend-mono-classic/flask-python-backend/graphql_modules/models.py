from datetime import datetime
from mongoengine import Document
from mongoengine.fields import (
    DateTimeField, ReferenceField, StringField, IntField, BooleanField, ObjectIdField,
)

class Comments(Document):
    meta = {'collection': 'comments'}
    ID = ObjectIdField()
    text = StringField()
    author = ReferenceField('Users') # Quotes are used to avoid failing of the class, that will be defined only below
    post = ReferenceField('Posts')
    date = DateTimeField(default=datetime.now)

class Users(Document):
    meta = {'collection': 'users'}
    ID = ObjectIdField()
    firstName = StringField()
    lastName = StringField()
    emailId = StringField()
    age = IntField()
    posts = ReferenceField('Posts')
    comments = ReferenceField(Comments)

class Posts(Document):
    meta = {'collection': 'posts'}
    ID = ObjectIdField()
    title = StringField()
    body = StringField()
    published = BooleanField()
    author = ReferenceField(Users)
    comments = ReferenceField(Comments)
    date = DateTimeField(default=datetime.now)