import graphene
from graphene.relay import Node
from graphene_mongo.fields import MongoengineConnectionField
from .types import PostsType, CommentsType, UsersType
from .models import Users, Posts, Comments
from .mutations import CreateCommentsMutation, UpdateCommentsMutation, DeleteCommentsMutation, CreateUsersMutation, UpdateUsersMutation, DeleteUsersMutation, CreatePostsMutation, UpdatePostsMutation, DeletePostsMutation

class Mutations(graphene.ObjectType):
    create_users = CreateUsersMutation.Field()
    update_users = UpdateUsersMutation.Field()
    delete_users = DeleteUsersMutation.Field()
    create_posts = CreatePostsMutation.Field()
    update_posts = UpdatePostsMutation.Field()
    delete_posts = DeletePostsMutation.Field()
    create_comments = CreateCommentsMutation.Field()
    update_comments = UpdateCommentsMutation.Field()
    delete_comments = DeleteCommentsMutation.Field()

class Query(graphene.ObjectType):
    node = Node.Field()
    all_users = MongoengineConnectionField(UsersType)
    all_posts = MongoengineConnectionField(PostsType)
    all_comments = MongoengineConnectionField(CommentsType)
    users_list = graphene.Field(UsersType)
    posts_list = graphene.Field(PostsType)
    comments_list = graphene.Field(CommentsType)
    
    def resolve_users_list(self, info):
        return Users.objects.all()

    def resolve_posts_list(self, info):
        return Posts.objects.all()

    def resolve_comments_list(self, info):
        return Comments.objects.all()


schema = graphene.Schema(query=Query, mutation=Mutations, types=[UsersType, PostsType, CommentsType])