from graphene import relay
from graphene_mongo import MongoengineObjectType
from .models import Users, Posts, Comments


class PostsType(MongoengineObjectType):
    class Meta:
        model = Posts
        interfaces = (relay.Node,)

class CommentsType(MongoengineObjectType):
    class Meta:
        model = Comments
        interfaces = (relay.Node,)

class UsersType(MongoengineObjectType):
    class Meta:
        model = Users
        interfaces = (relay.Node,)