Mutations:

mutation createUser {
  createUsers(usersData: {firstName: "Hello", lastName: "World", emailId: "bala@com", age: 23}) {
    users {
      firstName
      lastName
      emailId
      age
    }
  }
}

mutation updateUser {
  updateUsers(usersData: {id: "62c2ea18b7a9ef2109ca074f", firstName: "NO Hello"}) {
    users {
      id
      firstName
    }
  }
}

mutation deleteUser {
  deleteUsers(id: "62c2ea18b7a9ef2109ca074f") {
    success
  }
}

mutation createPost {
  createPosts(postsData: {title: "Hello", body: "First Post", published: true, author: "62c33bf1f9e67a06086dc613"}){
    posts{
      title
      body
      published
      date
    }
  }
}

mutation createComment {
  createComments(commentsData: {text: "Comment under the First Post", author: "62c33bf1f9e67a06086dc613", post: "62c34e6b576f9f42168ed17d"}){
    comments{
      text
    }
  }
}


#################################

Queries:

query {
  allUsers{edges
  {node{
    firstName
  }}}
}


query{
  allComments{
    edges{
      node{
        text
        post{
          body
        }
      }
    }
  }
}
