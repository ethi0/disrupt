import graphene
from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist
from .models import Users, Posts, Comments
from .types import UsersType, PostsType, CommentsType

#####################################################  USER SECTION


class UsersInput(graphene.InputObjectType):
    id = graphene.ID()
    firstName = graphene.String()
    lastName = graphene.String()
    emailId = graphene.String()
    age = graphene.Int()


class CreateUsersMutation(graphene.Mutation):
    users = graphene.Field(UsersType)

    class Arguments:
        users_data = UsersInput(required=True)

    def mutate(self, info, users_data=None):
        users = Users(
            firstName=users_data.firstName,
            lastName=users_data.lastName,
            emailId=users_data.emailId,
            age=users_data.age
        )
        users.save()

        return CreateUsersMutation(users=users)


class UpdateUsersMutation(graphene.Mutation):
    users = graphene.Field(UsersType)

    class Arguments:
        users_data = UsersInput(required=True)

    @staticmethod
    def get_object(id):
        return Users.objects.get(pk=id)

    def mutate(self, info, users_data=None):
        users = UpdateUsersMutation.get_object(users_data.id)
        if users_data.firstName:
            users.firstName = users_data.firstName
        if users_data.lastName:
            users.lastName = users_data.lastName
        if users_data.emailId:
            users.emailId = users_data.emailId
        if users_data.age:
            users.age = users_data.age

        users.save()

        return UpdateUsersMutation(users=users)


class DeleteUsersMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    success = graphene.Boolean()

    def mutate(self, info, id):
        try:
            Users.objects.get(pk=id).delete()
            success = True
        except ObjectDoesNotExist:
            success = False

        return DeleteUsersMutation(success=success)

###########################################################  POSTS SECTION


class PostsInput(graphene.InputObjectType):
    id = graphene.ID()
    title = graphene.String()
    body = graphene.String()
    published = graphene.Boolean()
    author = graphene.String()                  ## NEED FURTHER INVESTIGATION - GETTING PROBLEMS HERE on backend about objectID
    comments = graphene.String()                ## NEED FURTHER INVESTIGATION
    date = graphene.types.datetime.Date() 


class CreatePostsMutation(graphene.Mutation):
    posts = graphene.Field(PostsType)

    class Arguments:
        posts_data = PostsInput(required=True)

    def mutate(self, info, posts_data=None):
        posts = Posts(
            title=posts_data.title,
            body=posts_data.body,
            published=posts_data.published,
            author=posts_data.author,
            comments=posts_data.comments,
            date=posts_data.date
        )
        posts.save()

        return CreatePostsMutation(posts=posts)


class UpdatePostsMutation(graphene.Mutation):
    posts = graphene.Field(PostsType)

    class Arguments:
        posts_data = PostsInput(required=True)

    @staticmethod
    def get_object(id):
        return Posts.objects.get(pk=id)

    def mutate(self, info, posts_data=None):
        posts = UpdatePostsMutation.get_object(posts_data.id)
        if posts_data.title:
            posts.title = posts_data.title
        if posts_data.body:
            posts.body = posts_data.body
        if posts_data.published:
            posts.published = posts_data.published

        posts.save()

        return UpdatePostsMutation(posts=posts)


class DeletePostsMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    success = graphene.Boolean()

    def mutate(self, info, id):
        try:
            Posts.objects.get(pk=id).delete()
            success = True
        except ObjectDoesNotExist:
            success = False

        return DeletePostsMutation(success=success)


###################################################  COMMENTS SECTION

class CommentsInput(graphene.InputObjectType):
    id = graphene.ID()
    text = graphene.String()
    author = graphene.String()                  ## NEED FURTHER INVESTIGATION
    post = graphene.String()                ## NEED FURTHER INVESTIGATION
    date = graphene.types.datetime.Date()       ## NEED FURTHER INVESTIGATION


class CreateCommentsMutation(graphene.Mutation):
    comments = graphene.Field(CommentsType)

    class Arguments:
        comments_data = CommentsInput(required=True)

    def mutate(self, info, comments_data=None):
        comments = Comments(
            text=comments_data.text,
            author=comments_data.author,
            post=comments_data.post,
            date=comments_data.date
        )
        comments.save()

        return CreateCommentsMutation(comments=comments)


class UpdateCommentsMutation(graphene.Mutation):
    comments = graphene.Field(CommentsType)

    class Arguments:
        comments_data = CommentsInput(required=True)

    @staticmethod
    def get_object(id):
        return Comments.objects.get(pk=id)

    def mutate(self, info, comments_data=None):
        comments = UpdateCommentsMutation.get_object(comments_data.id)
        if comments_data.text:
            comments.text = comments_data.text

        comments.save()

        return UpdateCommentsMutation(comments=comments)


class DeleteCommentsMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    success = graphene.Boolean()

    def mutate(self, info, id):
        try:
            Comments.objects.get(pk=id).delete()
            success = True
        except ObjectDoesNotExist:
            success = False

        return DeleteCommentsMutation(success=success)