#!/usr/bin/env python
import pika, sys, os, pymongo, bsonjs
from bson.raw_bson import RawBSONDocument
from bson import json_util
from gridfs import Database
import json

credentials = pika.PlainCredentials('root', 'password')
parameters = pika.ConnectionParameters('rabbitmq', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='rpc_queue_mongo_get')
uri = 'mongodb://root:password@mongo:27017/test?authSource=admin&authMechanism=SCRAM-SHA-256' 

def mongo_write(data):
    client = pymongo.MongoClient(uri)
    db = client.get_default_database()
    songs = db['songs']
    cursor = songs.find({})
    array = []
    for document in cursor:
        array.append(document)
    client.close()
    return json.loads(json_util.dumps(array))

def on_request(ch, method, props, body):

    print(" [.] mongo will (%s)" % body)
    response = mongo_write(body)
    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = \
                                                         props.correlation_id),
                     body=json.dumps(response))
    ch.basic_ack(delivery_tag=method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='rpc_queue_mongo_get', on_message_callback=on_request)

print(" [x] Awaiting RPC requests")
channel.start_consuming()