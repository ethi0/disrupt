#!/usr/bin/env python
import pika

credentials = pika.PlainCredentials('root', 'password')
parameters = pika.ConnectionParameters('rabbitmq', 5672, '/', credentials)

connection = pika.BlockingConnection(parameters)
channel = connection.channel()

channel.queue_declare(queue='SIG_WRITE')

channel.basic_publish(exchange='', routing_key='SIG_WRITE', body='Hello Microservice B! Please write something to database and leave a message for Microservice C!')
print(" [x] Sent 'Message From A to B'")
connection.close()