#!/usr/bin/env python
import pika
import uuid
import json


class MongoRpcClient(object):

    def __init__(self):

        credentials = pika.PlainCredentials('root', 'password')
        parameters = pika.ConnectionParameters('rabbitmq', 5672, '/', credentials)

        self.connection = pika.BlockingConnection(parameters)


        self.channel = self.connection.channel()

        result = self.channel.queue_declare(queue='', exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self, n):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key=rpc_req,
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
            ),
            body=n)
        while self.response is None:
            self.connection.process_data_events()
        return self.response


mongo_rpc = MongoRpcClient()
rpc_req = 'rpc_queue_mongo_del'

print(" [x] Sending request %r" % rpc_req)
response = mongo_rpc.call(rpc_req)
print(" [.] Done: %r" % response)