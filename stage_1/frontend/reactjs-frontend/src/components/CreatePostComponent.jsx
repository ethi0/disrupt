import React, { Component } from 'react'
import PostService from '../services/PostService'

class CreatePostComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            title: '',
            body: '',
            author: '',
        }
        this.changeTitleHandler = this.changeTitleHandler.bind(this);
        this.changeBodyHandler = this.changeBodyHandler.bind(this);
        this.saveOrUpdatePost = this.saveOrUpdatePost.bind(this);
    }

    // step 3
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            PostService.getPostById(this.state.id).then( (res) =>{
                let post = res.data;
                this.setState({title: post.title,
                    body: post.body,
                    author : post.author
                });
            });
        }        
    }
    saveOrUpdatePost = (e) => {
        e.preventDefault();
        let post = {title: this.state.title, body: this.state.body, author: this.state.author};
        console.log('post => ' + JSON.stringify(post));

        // step 5
        if(this.state.id === '_add'){
            PostService.createPost(post).then(res =>{
                this.props.history.push('/posts');
            });
        }else{
            PostService.updatePost(post, this.state.id).then( res => {
                this.props.history.push('/posts');
            });
        }
    }
    
    changeTitleHandler= (event) => {
        this.setState({title: event.target.value});
    }

    changeBodyHandler= (event) => {
        this.setState({body: event.target.value});
    }

    changeAuthorHandler= (event) => {
        this.setState({author: event.target.value});
    }

    cancel(){
        this.props.history.push('/posts');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Add Post</h3>
        }else{
            return <h3 className="text-center">Update Post</h3>
        }
    }
    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                {
                                    this.getTitle()
                                }
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> Title: </label>
                                            <input placeholder="Title" name="title" className="form-control" 
                                                value={this.state.title} onChange={this.changeTitleHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Body: </label>
                                            <input placeholder="Body" name="body" className="form-control" 
                                                value={this.state.body} onChange={this.changeBodyHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Author: </label>
                                            <input placeholder="Author" name="author" className="form-control" 
                                                value={this.state.author} onChange={this.changeAuthorHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.saveOrUpdatePost}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default CreatePostComponent
