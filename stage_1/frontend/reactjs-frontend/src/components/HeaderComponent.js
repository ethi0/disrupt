import React, { Component } from 'react'
import'bootstrap/dist/css/bootstrap.min.css'

class HeaderComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                 
        }
    }

    render() {
        return (
            <div>
                <header>
                    <nav className="navbar navbar-dark bg-primary">
                    <div><a href="/users" className="navbar-brand">User Management</a></div>
                    <div><a href="/posts" className="navbar-brand">Post Management</a></div>
                    <div><a href="/comments" className="navbar-brand">Comment Management</a></div>
                    <div><a href="/graphqlreq/users" className="navbar-brand">GQL U-management</a></div>
                    <div><a href="/graphqlreq/posts" className="navbar-brand">GQL P-management</a></div>
                    <div><a href="/graphqlreq/comments" className="navbar-brand">GQL C-management</a></div>
                    <div><a href="/distr/users" className="navbar-brand">DISTR U-management</a></div>
                    <div><a href="/distr/posts" className="navbar-brand">DISTR P-management</a></div>
                    <div><a href="/distr/comments" className="navbar-brand">DISTR C-management</a></div>
                    <div><a href="/" className="navbar-brand">HOME</a></div>
                    </nav>
                </header>
            </div>
        )
    }
}

export default HeaderComponent
