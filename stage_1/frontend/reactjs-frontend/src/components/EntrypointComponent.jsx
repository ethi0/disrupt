import React, { Component } from 'react'
import '../static/css/main.css'
class EntrypointComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                 
        }
    }

    render() {
        return (
            <div className="main">
            <header>
            <h3><a href="tel:+32432892938539"><font color="#AFE8DF">Phone: +32432892938539</font></a></h3>
            <h3><a href="mailto:support@disrupt.org.ua"><font color="#AFE8DF">Email: support@disrupt.org.ua</font></a></h3>
            <h3><a href="//t.me/nothingBot"><font color="#AFE8DF">Telegram: @nothingBot</font></a></h3>
            </header>
            <h1>Welcome to DISRUPT!</h1>
            <h2>HTML + CSS + REACTJS</h2>
            <h2>NGINX + Python Flask + MongoDB</h2>
            <p>Please choose your way...</p>
            <p><a href="/users"><font color="#565d66">User Management</font></a></p>
            <p><a href="/posts"><font color="#565d66">Post Management</font></a></p>
            <p><a href="/comments"><font color="#565d66">Comment Management</font></a></p>
            <p><a href="/graphqlreq/users"><font color="#565d66">GQL U-management</font></a></p>
            <p><a href="/graphqlreq/posts"><font color="#565d66">GQL P-management</font></a></p>
            <p><a href="/graphqlreq/comments"><font color="#565d66">GQL C-management</font></a></p>
            <p><a href="/distr/users"><font color="#565d66">DISTR U-management</font></a></p>
            <p><a href="/distr/posts"><font color="#565d66">DISTR P-management</font></a></p>
            <p><a href="/distr/comments"><font color="#565d66">DISTR C-management</font></a></p>
            <p><a href="http://disrupt.org.ua:15672">Rabbit-MQ (root:password)</a></p>
            <p><a href="http://disrupt.org.ua:8500">Consul (maintenance)</a></p>
        </div>
        )
    }
}

export default EntrypointComponent