import React, { Component } from 'react'
import PostService from '../services/PostService'

class ViewPostComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            post: {}
        }
    }

    componentDidMount(){
        PostService.getPostById(this.state.id).then( res => {
            this.setState({post: res.data});
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> View Post Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <label> Post Title: </label>
                            <div> { this.state.post.title }</div>
                        </div>
                        <div className = "row">
                            <label> Post Body: </label>
                            <div> { this.state.post.body }</div>
                        </div>
                        <div className = "row">
                            <label> Post Author: </label>
                            <div> { this.state.post.author }</div>
                        </div>
                        <div className = "row">
                            <label> Post Date: </label>
                            <div> { this.state.post.date }</div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default ViewPostComponent
