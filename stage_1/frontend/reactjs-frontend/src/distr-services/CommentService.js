import axios from 'axios';

const USER_API_BASE_URL = "http://disrupt.org.ua:5001/distr/comments";

class DistrCommentService {

    getComments(){
        return axios.get(USER_API_BASE_URL, + '/comments');
    }
    createComment(comment){
        return axios.post(USER_API_BASE_URL, comment);
    }
    getCommentById(commentId){
        return axios.get(USER_API_BASE_URL + '/' + commentId);
    }
    updateComment(comment, commentId){
        return axios.put(USER_API_BASE_URL + '/' + commentId, comment);
    }
    deleteComment(commentId){
        return axios.delete(USER_API_BASE_URL + '/' + commentId);
    }
}

export default new DistrCommentService()