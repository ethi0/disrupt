import axios from 'axios';

const USER_API_BASE_URL = "http://disrupt.org.ua:5000/graphqlreq/posts";

class PostService {

    getPosts(){
        return axios.get(USER_API_BASE_URL, + '/posts');
    }
    createPost(post){
        return axios.post(USER_API_BASE_URL, post);
    }
    getPostById(postId){
        return axios.get(USER_API_BASE_URL + '/' + postId);
    }
    updatePost(post, postId){
        return axios.put(USER_API_BASE_URL + '/' + postId, post);
    }
    deletePost(postId){
        return axios.delete(USER_API_BASE_URL + '/' + postId);
    }
}

export default new PostService()