import React from 'react';
// eslint-disable-next-line
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import ListUserComponent from './components/ListUserComponent';
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import CreateUserComponent from './components/CreateUserComponent';
// eslint-disable-next-line
// import UpdateUserComponent from './components/UpdateUserComponent';
import ViewUserComponent from './components/ViewUserComponent';
import ListPostComponent from './components/ListPostComponent';
import CreatePostComponent from './components/CreatePostComponent';
import ViewPostComponent from './components/ViewPostComponent';
import ListCommentComponent from './components/ListCommentComponent';
import CreateCommentComponent from './components/CreateCommentComponent';
import ViewCommentComponent from './components/ViewCommentComponent';
import GqlListUserComponent from './graphql-components/ListUserComponent';
import GqlCreateUserComponent from './graphql-components/CreateUserComponent';
import GqlViewUserComponent from './graphql-components/ViewUserComponent';
import GqlListPostComponent from './graphql-components/ListPostComponent';
import GqlCreatePostComponent from './graphql-components/CreatePostComponent';
import GqlViewPostComponent from './graphql-components/ViewPostComponent';
import GqlListCommentComponent from './graphql-components/ListCommentComponent';
import GqlCreateCommentComponent from './graphql-components/CreateCommentComponent';
import GqlViewCommentComponent from './graphql-components/ViewCommentComponent';
import EntrypointComponent from './components/EntrypointComponent';
import DistrListUserComponent from './distr-components/ListUserComponent';
import DistrCreateUserComponent from './distr-components/CreateUserComponent';
import DistrViewUserComponent from './distr-components/ViewUserComponent';


function App() {
  return (
    <div>
        <Router>
              <HeaderComponent />
                <div style = {{ padding: '0px 40px', position: 'relative' }}>
                    <Switch> 
                          <Route path = "/" exact component = {EntrypointComponent}></Route>
                          <Route path = "/users" component = {ListUserComponent}></Route>
                          <Route path = "/add-user/:id" component = {CreateUserComponent}></Route>
                          <Route path = "/view-user/:id" component = {ViewUserComponent}></Route>
                          <Route path = "/posts" component = {ListPostComponent}></Route>
                          <Route path = "/add-post/:id" component = {CreatePostComponent}></Route>
                          <Route path = "/view-post/:id" component = {ViewPostComponent}></Route>
                          <Route path = "/comments" component = {ListCommentComponent}></Route>
                          <Route path = "/add-comment/:id" component = {CreateCommentComponent}></Route>
                          <Route path = "/view-comment/:id" component = {ViewCommentComponent}></Route>
                          <Route path = "/graphqlreq/users" component = {GqlListUserComponent}></Route>
                          <Route path = "/graphqlreq/add-user/:id" component = {GqlCreateUserComponent}></Route>
                          <Route path = "/graphqlreq/view-user/:id" component = {GqlViewUserComponent}></Route>
                          <Route path = "/graphqlreq/posts" component = {GqlListPostComponent}></Route>
                          <Route path = "/graphqlreq/add-post/:id" component = {GqlCreatePostComponent}></Route>
                          <Route path = "/graphqlreq/view-post/:id" component = {GqlViewPostComponent}></Route>
                          <Route path = "/graphqlreq/comments" component = {GqlListCommentComponent}></Route>
                          <Route path = "/graphqlreq/add-comment/:id" component = {GqlCreateCommentComponent}></Route>
                          <Route path = "/graphqlreq/view-comment/:id" component = {GqlViewCommentComponent}></Route>
                          <Route path = "/distr/users" component = {DistrListUserComponent}></Route>
                          <Route path = "/distr/add-user/:id" component = {DistrCreateUserComponent}></Route>
                          <Route path = "/view-user/:id" component = {DistrViewUserComponent}></Route>
                          {/* <Route path = "/update-user/:id" component = {UpdateUserComponent}></Route> */}
                    </Switch>
                </div>
              <FooterComponent />
        </Router>
    </div>
    
  );
}

export default App;
