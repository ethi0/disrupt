import React, { Component } from 'react'
import CommentService from '../graphql-services/CommentService'

class GqlCreateCommentComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            text: '',
            author: '',
            post: '',
        }
        this.changeTextHandler = this.changeTextHandler.bind(this);
        this.changeAuthorHandler = this.changeAuthorHandler.bind(this);
        this.saveOrUpdateComment = this.saveOrUpdateComment.bind(this);
    }

    // step 3
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            CommentService.getCommentById(this.state.id).then( (res) =>{
                let comment = res.data;
                this.setState({text: comment.text,
                    author: comment.author,
                    post : comment.post
                });
            });
        }        
    }
    saveOrUpdateComment = (e) => {
        e.preventDefault();
        let comment = {text: this.state.text, author: this.state.author, post: this.state.post};
        console.log('comment => ' + JSON.stringify(comment));

        // step 5
        if(this.state.id === '_add'){
            CommentService.createComment(comment).then(res =>{
                this.props.history.push('/comments');
            });
        }else{
            CommentService.updateComment(comment, this.state.id).then( res => {
                this.props.history.push('/comments');
            });
        }
    }
    
    changeTextHandler= (event) => {
        this.setState({text: event.target.value});
    }

    changeAuthorHandler= (event) => {
        this.setState({author: event.target.value});
    }

    changePostHandler= (event) => {
        this.setState({post: event.target.value});
    }

    cancel(){
        this.props.history.push('/comments');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Add Comment</h3>
        }else{
            return <h3 className="text-center">Update Comment</h3>
        }
    }
    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                {
                                    this.getTitle()
                                }
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> Text: </label>
                                            <input placeholder="Text" name="text" className="form-control" 
                                                value={this.state.text} onChange={this.changeTextHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Author: </label>
                                            <input placeholder="Author" name="author" className="form-control" 
                                                value={this.state.author} onChange={this.changeAuthorHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Post: </label>
                                            <input placeholder="Post" name="post" className="form-control" 
                                                value={this.state.post} onChange={this.changePostHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.saveOrUpdateComment}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default GqlCreateCommentComponent
